README for Social Media Player

In application.yml Change the clientId and clientSecret to your own instagram client.

- While in Instagram sandbox you will need to invite the users you want to have access to your app.
- I recommend not using more than one user, otherwise Instagram will start spamming you for authentication codes which is 1) annoying, and 2) breaks the OAuth redirect process.
- Each invited user has to register a developer account and accept the invite under their own "Sandbox Invites". From what I can see Instagram something forgets who has accepted the invited.

