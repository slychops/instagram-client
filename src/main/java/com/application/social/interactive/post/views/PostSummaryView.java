package com.application.social.interactive.post.views;


import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class PostSummaryView {

    List<PostView> posts;

}
