package com.application.social.interactive.post.mappers;

import com.application.social.interactive.post.response.Posts;
import com.application.social.interactive.post.views.PostSummaryView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostSummaryViewMapper {

    private final PostViewMapper postViewMapper;

    public PostSummaryView map(Posts posts) {

        return PostSummaryView.builder()
                .posts(postViewMapper.map(posts.getSinglePost()))
                .build();
    }

}
