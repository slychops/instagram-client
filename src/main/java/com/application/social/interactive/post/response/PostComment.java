package com.application.social.interactive.post.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PostComment {

    @JsonAlias("createdTime")
    private String createdTime;
    private String text;
    private User from;
    private String id;

}
