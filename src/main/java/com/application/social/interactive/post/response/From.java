package com.application.social.interactive.post.response;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class From {

    private String id;
    private String full_name;
    @JsonAlias("profile_picture")
    private String profilePicture;
    private String username;

}
