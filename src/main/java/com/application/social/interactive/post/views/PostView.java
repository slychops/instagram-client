package com.application.social.interactive.post.views;

import com.application.social.interactive.post.response.Image;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PostView {

    private String id;
    private Image image;

}
