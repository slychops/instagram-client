package com.application.social.interactive.post.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Post {

    private String id;
    private User user;
    private Images images;
    @JsonAlias("createdTime")
    private String createdTime;
    private Caption caption;
    @JsonAlias("user_has_liked")
    private boolean userHasLiked;
    private Likes likes;
    private List<Object> tags;
    private String filter;
    private Comments comments;
    private String type;
    private String link;

}

