package com.application.social.interactive.post.response;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Caption {

    private String id;
    private String text;
    @JsonAlias("created_time")
    private String createdTime;
    private From from;

}
