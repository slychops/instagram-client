package com.application.social.interactive.post.mappers;

import com.application.social.interactive.post.response.Post;
import com.application.social.interactive.post.views.PostView;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PostViewMapper {

    public PostView map(Post post) {
        return PostView.builder()
                .id(post.getId())
                .image(post.getImages().getThumbnail())
                .build();
    }

    public List<PostView> map(List<Post> input) {

        return input.stream().map(this::map).collect(Collectors.toList());
    }
}
