package com.application.social.interactive.post.response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class User {

    private String id;
    private String full_name;
    private String profile_picture;
    private String username;

}
