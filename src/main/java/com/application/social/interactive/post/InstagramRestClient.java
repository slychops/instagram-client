package com.application.social.interactive.post;

import com.application.social.interactive.post.response.PostComments;
import com.application.social.interactive.post.response.Posts;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.application.social.interactive.post.AccessToken.getToken;

@Component
@NoArgsConstructor
public class InstagramRestClient {

    private RestTemplate restTemplate = new RestTemplate();

    public Posts getAllPosts() {

        return restTemplate.getForObject(getUrl(), Posts.class);
    }

    private String getUrl(){
        return "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + getToken();
    }

    public PostComments getPostComments(String id) {
        String commentsEndPoint = "https://api.instagram.com/v1/media/" + id + "/comments?access_token=" + getToken();
        PostComments postComments = restTemplate.getForObject(commentsEndPoint, PostComments.class);

        return postComments;
    }

}
