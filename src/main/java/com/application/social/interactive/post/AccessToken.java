package com.application.social.interactive.post;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class AccessToken {

    static String getToken() {
        String token = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {

            token = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
        }

        return token;
    }

}
