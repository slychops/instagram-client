package com.application.social.interactive.post.mappers;

import com.application.social.interactive.post.response.Post;
import com.application.social.interactive.post.response.PostComments;
import com.application.social.interactive.post.views.PostDetailView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostDetailViewMapper {

    public PostDetailView map(Post post, PostComments postComments) {

        return PostDetailView.builder()
                .id(post.getId())
                .image(post.getImages().getStandardResolution())
                .username(post.getUser().getUsername())
                .avatar(post.getUser().getProfile_picture())
                .caption(post.getCaption().getText())
                .comments(postComments.getSingleComment())
                .createdTime(post.getCreatedTime())
                .build();
    }

}
