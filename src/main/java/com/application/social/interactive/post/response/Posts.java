package com.application.social.interactive.post.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Posts {

    private Object pagination;
    @JsonAlias("data")
    private List<Post> singlePost;
    private Meta meta;

    public Post getPostById(String id) {
        return this.getSinglePost().stream()
                .filter(post -> post.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No post with id: " + id));
    }

}