package com.application.social.interactive.post.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Images {

    private Image thumbnail;
    @JsonAlias("low_resolution")
    private Image lowResolution;
    @JsonAlias("standard_resolution")
    private Image standardResolution;

}
