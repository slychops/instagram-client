package com.application.social.interactive.post;

import com.application.social.interactive.post.mappers.PostDetailViewMapper;
import com.application.social.interactive.post.mappers.PostSummaryViewMapper;
import com.application.social.interactive.post.response.Post;
import com.application.social.interactive.post.response.PostComments;
import com.application.social.interactive.post.response.Posts;
import com.application.social.interactive.post.views.PostDetailView;
import com.application.social.interactive.post.views.PostSummaryView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostService {

    private final PostSummaryViewMapper postSummaryViewMapper;
    private final PostDetailViewMapper postDetailViewMapper;
    private final InstagramRestClient instagramRestClient;

    public PostSummaryView getUserPosts() {
        Posts posts = instagramRestClient.getAllPosts();

        return postSummaryViewMapper.map(posts);
    }

    public PostDetailView postDetailView(String id) {
        Posts posts = instagramRestClient.getAllPosts();
        Post selectedPost = posts.getPostById(id);
        PostComments postComments = instagramRestClient.getPostComments(id);

        return postDetailViewMapper.map(selectedPost, postComments);
    }

}
