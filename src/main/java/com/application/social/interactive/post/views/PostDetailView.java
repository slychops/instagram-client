package com.application.social.interactive.post.views;

import com.application.social.interactive.post.response.Image;
import com.application.social.interactive.post.response.PostComment;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class PostDetailView {

    private String id;
    private Image image;
    private String username;
    private String avatar;
    private String caption;
    private String createdTime;
    private List<PostComment> comments;

}

