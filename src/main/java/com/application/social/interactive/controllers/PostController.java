package com.application.social.interactive.controllers;


import com.application.social.interactive.post.PostService;
import com.application.social.interactive.post.views.PostDetailView;
import com.application.social.interactive.post.views.PostSummaryView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
@Slf4j
public class PostController {

    private final PostService postService;

    @GetMapping
    public PostSummaryView getAllPosts() {

        log.info("In getAllPosts ControlMapping");
        return postService.getUserPosts();
    }

    @GetMapping("/{id}")
    public PostDetailView getOnePost(@PathVariable String id) {

        log.info("In getOnePost ControlMapping");
        return postService.postDetailView(id);
    }

}
