var posts;
var postId;
var postUrl;
var onePost;
var imageClicked;

$.get("/post", function(data) {
        posts = data.posts;

        posts.forEach(function (index) {
            postId = index.id;
            postUrl = index.image.url;
            $("#insert-after").append('<div class="single-post" id="' + postId + '"><img src="' + postUrl + '">');
        })
    })

$("#insert-after").on("click", "div.single-post", function(){
    displayPostArea(this.id);
});

function displayPostArea(postId) {

    $.get("/post/" + postId, function(data) {
        onePost = data;
        $(".hidden-post-area").show();
        $("#back").show();
        displayPost();
    })
}

function displayPost() {

    $('#image-location').append('<img src="' + onePost.image.url + '">');
    $('#avatar').append('<img src="' + onePost.avatar + '" style="border-radius:50%">');
    $('#username').append(onePost.username);
    $('#caption').append(onePost.caption);

    onePost.comments.forEach(function (index) {
        $('#comments').append('<div class="one-comment">' + index.text + '</div>')
    })
}

