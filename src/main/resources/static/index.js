$.get("/user", function(data) {
    if (typeof data === 'object') {
        $("#user").html(data.userAuthentication.details.data.username);
        $("#token").html(data.details.tokenValue);
        $("#profile-pic").append('<img src="' + data.userAuthentication.details.data.profile_picture + '" style="border-radius: 50%" />');
        $(".unauthenticated").hide()
        $(".authenticated").show()
    }
});



var logout = function() {
    $.post("/logout", function() {
        $("#user").html('');
        $(".unauthenticated").show();
        $(".authenticated").hide();
    })
    return true;
}



