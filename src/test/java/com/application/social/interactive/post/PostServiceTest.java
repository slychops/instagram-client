package com.application.social.interactive.post;

import com.application.social.interactive.post.mappers.PostDetailViewMapper;
import com.application.social.interactive.post.mappers.PostSummaryViewMapper;
import com.application.social.interactive.post.response.Post;
import com.application.social.interactive.post.response.PostComments;
import com.application.social.interactive.post.response.Posts;
import com.application.social.interactive.post.views.PostDetailView;
import com.application.social.interactive.post.views.PostSummaryView;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceTest {

    private static final String ID = "id";

    @Mock
    private PostSummaryViewMapper postSummaryViewMapper;

    @Mock
    private PostDetailViewMapper postDetailViewMapper;

    @Mock
    private InstagramRestClient instagramRestClient;

    @InjectMocks
    private PostService sut;

    @Mock
    private Posts posts;

    @Mock
    private Post post;

    @Mock
    private PostComments postComments;

    @Mock
    private PostDetailView postDetailView;

    @Mock
    private PostSummaryView postSummaryView;

    @Before
    public void before() {
        when(instagramRestClient.getAllPosts()).thenReturn(posts);
    }

    @Test
    public void when_getUserPosts_expect_services_called_and_PostSummaryView_returned() {
        when(postSummaryViewMapper.map(posts)).thenReturn(postSummaryView);
        PostSummaryView result = sut.getUserPosts();

        assertThat(result).isEqualTo(postSummaryView);
    }

    @Test
    public void when_postDetailView_expect_services_called_and_PostDetailView_returned(){
        when(posts.getPostById(ID)).thenReturn(post);
        when(instagramRestClient.getPostComments(ID)).thenReturn(postComments);
        when(postDetailViewMapper.map(post, postComments)).thenReturn(postDetailView);

        PostDetailView result = sut.postDetailView(ID);

        assertThat(result).isEqualTo(postDetailView);

    }

}