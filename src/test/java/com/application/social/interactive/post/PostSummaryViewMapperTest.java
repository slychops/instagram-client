package com.application.social.interactive.post;

import com.application.social.interactive.post.mappers.PostSummaryViewMapper;
import com.application.social.interactive.post.mappers.PostViewMapper;
import com.application.social.interactive.post.response.Post;
import com.application.social.interactive.post.response.Posts;
import com.application.social.interactive.post.views.PostSummaryView;
import com.application.social.interactive.post.views.PostView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostSummaryViewMapperTest {

    @Mock
    private PostViewMapper postViewMapper;

    @InjectMocks
    private PostSummaryViewMapper sut;

    @Mock
    private List<Post> postList;

    @Mock
    private List<PostView> postViewList;

    @Test
    public void when_map_called_result_expected() {
        when(postViewMapper.map(postList)).thenReturn(postViewList);

        Posts input = new Posts();

        PostSummaryView result = sut.map(input);

        assertThat(result.getPosts()).isEqualTo(postViewList);

    }

}